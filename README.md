
<p align="center">
    <h1 align="center">Duplicate File Check</h1>

  <p align="center">
    PHP Application to check duplicate file
  </p>
</p>



## About

A simple PHP application to check duplicate file in a directory.


Please feel free to contribute, open issues or just enjoy this application!


## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

1.  PHP Version 7.2.6
2.  Apache 2.0

### Installation

1.  Clone or download the repo
```sh
git clone https://gitlab.com/hkmuttaqin/php-duplicate-file-check.git
```
2. Move the folder to your application folder directory i.e : C:/XAMPP/htdocs
3. Edit file  config.php to change the directory that will be used. Default directory is DropsuiteTest
4. Open your application url. i.e http://localhost/php-duplicate-file-check
5. Enjoy!
