<?php
// include config file for searching root directory
require_once 'config.php';

$result = array(); 
$combination = array();

checkDuplicateFile($dir);

function checkDuplicateFile($dir){
    //get all files from root directory
    $fileArray = listFolderFiles($dir);

    //get all possible combination of 2 array
    $arrayCombination = getCombinationToCompare($fileArray, 2);

    //get all duplicate array
    $compareArrayCombinationResult = getDuplicateArray($arrayCombination);

    //get unique duplicate array
    $duplicateArrayValue = getDuplicateArrayValue($compareArrayCombinationResult);

    //count duplicate array file
    $countContentResult = countContentResult($duplicateArrayValue);

    //print maximum duplicate file
    printOutputMax($countContentResult);
}


function listFolderFiles($dir){
    $ffs = scandir($dir);
    $fileArray = array();

    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1)
        return;

    foreach($ffs as $ff){        
        if(is_dir($dir.'/'.$ff)){
            $fileArray = array_merge($fileArray, listFolderFiles($dir.'/'.$ff));
        }else{
            $thisDir = $dir . '/' . $ff;
            $fileContent = file_get_contents($thisDir);
            $size = filesize($thisDir);
            $hash = md5_file ($thisDir);
            $content = file_get_contents($thisDir, FALSE, NULL, 0, 10);
            $tempArray = array(
                "dir" => $thisDir,
                "name" => $ff,
                "size" => $size,
                "hash" => $hash,
                "content" => $content
            );
        array_push($fileArray, $tempArray);
        }
    }
    return $fileArray;
}

function getCombinationToCompare(array $array, $choose) {
  global $result, $combination;

  $n = count($array);

  function inner ($start, $choose_, $arr, $n) {
    global $result, $combination;

    if ($choose_ == 0) array_push($result,$combination);
    else for ($i = $start; $i <= $n - $choose_; ++$i) {
        array_push($combination, $arr[$i]);
        inner($i + 1, $choose_ - 1, $arr, $n);
        array_pop($combination);
    }
  }
  inner(0, $choose, $array, $n);
  return $result;
}

function getDuplicateArray($arrayCombination){
    $result= array();
    foreach ($arrayCombination as $combine) {
        $isSameSizeResult = isSameSize($combine[0],$combine[1]);
        if ($isSameSizeResult) {
            $isSameHashResult = isSameHash($combine[0],$combine[1]);
            if ($isSameHashResult) {
                array_push($result, $combine[0]);
            }
        }
    }
    return $result;
}

function isSameSize($arrayA, $arrayB){
    $sizeA = $arrayA['size'];
    $sizeB = $arrayB['size'];
    
    if ($sizeA === $sizeB){
        return true;
    }
    else{
        return false;
    }
}

function isSameHash($arrayA, $arrayB){
    $hashA = $arrayA['hash'];
    $hashB = $arrayB['hash'];
    
    if ($hashA === $hashB){
        return true;
    }
    else{
        return false;
    }
}

function getDuplicateArrayValue($array){
    $result =array_map("unserialize", array_unique(array_map("serialize", $array)));
    return $result;
}

function countContentResult($array){
    $result = array_count_values(array_column($array, 'content'));
    return $result;
}

function printOutputMax($array){
    $arrayKey = getMaxArrayKey($array);
    $arrayValue = getMaxArrayValue($array);

    print $arrayKey . ' ' . $arrayValue;
}

function getMaxArrayKey($array){
    $result = array_keys($array, max($array));
    return $result[0];
}

function getMaxArrayValue($array){
    $result = max($array);
    return $result;
}
?>